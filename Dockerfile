FROM node:16.4.2-alpine
RUN mkdir -p /app/public
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 8085
CMD [ "node", "server.js" ]
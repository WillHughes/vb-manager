const config = require('./server/config');
const logger = require('./server/logger');
const app = require('./server/express');
const io = require('./server/socketio');
const http = require('http');
const server = http.createServer(app);

console.log = (data, ...args) => logger.debug(data + args);
console.warn = (data, ...args) => logger.warn(data + args);
console.error = (data, ...args) => logger.error(data + args);

const onError = (error) => {
	logger.error('http | ' + error);
	process.exit(1);
};

const onListening = () => {
	logger.info('http | listening on ' + config.host + ':' + config.port);
	io.attach(server);
};

const onHup = (signal) => {
	logger.info('EVENT RECEIVED: ' + signal);
};

process.on('SIGHUP', onHup);

process.on('SIGINT', () => {
	process.exit();
});

server.on('error', onError);
server.on('listening', onListening);
server.listen(config.port, config.host);

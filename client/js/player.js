const socket = io({
	transports: ['websocket']
});

socket.on('select', (data) => {
	if (data.type === 'image') startImage(data.file);
	if (data.type === 'video') startVideo(data.file, data.mime);
});

function startVideo(source, type, parentId) {
	try {
		parentId = parentId ? parentId : '#videoPlayer';
		$(parentId).attr('src', source);
		$(parentId).attr('type', type);
		$('#imageContainer').hide();
		$('#videoContainer').show();
	} catch (e) {
		console.log(e);
	}
}

function startImage(source, parentId) {
	try {
		parentId = parentId ? parentId : '#imagePlayer';
		$(parentId).attr('src', source);
		$('#videoContainer').hide();
		$('#imageContainer').show();
	} catch (e) {
		console.log(e);
	}
}

if ($('#play').attr('type') === 'image') startImage($('#play').attr('file'));
if ($('#play').attr('type') === 'video') startVideo($('#play').attr('file'), $('#play').attr('mime'));

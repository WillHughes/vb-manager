const logger = require('./logger');
const router = require('./router');
const express = require('express');
const expressWinston = require('express-winston');
const busboy = require('connect-busboy');
const favicon = require('serve-favicon');
const app = express();

app.set('trust proxy', 1);
app.set('view engine', 'pug');
app.set('views', './client/views');
app.set('query parser', 'simple');

app.use(expressWinston.logger({winstonInstance: logger, level: 'http'}));
app.use(express.static('dist'));
app.use(express.static('assets'));
app.use(express.static('public'));
app.use(favicon('assets/favicon.png'));
app.use(busboy({highWaterMark: 2 * 1024 * 1024}));
app.use(router);

module.exports = app;

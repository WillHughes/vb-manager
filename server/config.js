require('dotenv').config();

const config = new Object();
config.isProd = process.env.NODE_ENV === 'production' ? true : false;
config.host = process.env.HOST || '0.0.0.0';
config.port = process.env.PORT || '8085';

module.exports = config;

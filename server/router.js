const logger = require('./logger');
const io = require('./socketio');
const express = require('express');
const router = express.Router();
const mime = require('mime-types');
const fs = require('fs/promises');
const fss = require('fs');
const ffmpeg = require('fluent-ffmpeg');
const sharp = require('sharp');
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
ffmpeg.setFfmpegPath(ffmpegPath);
const active = new Object();
active.path = './public';
active.thumbdir = '/thumbnails';
active.file = '/default.jpg'
active.type = 'image'

const ffmpegTNSync = (input, output) => {
	return new Promise((resolve, reject) => {
		ffmpeg(input)
			.inputOptions(['-ss 00:00:01.00'])
			.outputOptions([
				'-vf scale=320:180:force_original_aspect_ratio=decrease',
				'-vframes 1'
			])
			.save(output)
			.on('end', () => {
				resolve();
			})
			.on('error', (err) => {
				reject(err);
			});
	});
};

const createThumbs = async (fileObj) => {
	if (fileObj.type === 'video') {
		try {
			await fs.stat(active.path + fileObj.thumb);
		} catch (e) {
			logger.debug('creating thumbnail ' + fileObj.thumb);
			await ffmpegTNSync(
				active.path + fileObj.file,
				active.path + fileObj.thumb
			);
		}
	}
	if (fileObj.type === 'image') {
		try {
			await fs.stat(active.path + fileObj.thumb);
		} catch (e) {
			logger.debug('creating thumbnail ' + fileObj.thumb);
			await sharp(active.path + fileObj.file)
				.resize(320, 180, {
					fit: 'cover'
				})
				.toFormat('jpg')
				.toFile(active.path + fileObj.thumb);
		}
	}
};

const loadFiles = async (req, res, next) => {
	try {
		await fs.mkdir(active.path + active.thumbdir, {recursive: true});
		const files = await fs.readdir(active.path, {withFileTypes: true});
		const filesArr = new Array();
		for (const file of files) {
			if (file.isFile()) {
				const fileObj = new Object();
				fileObj.file = '/' + file.name;
				fileObj.mime = await mime.lookup(active.path + fileObj.file);
				fileObj.type = String(fileObj.mime).split('/').shift();
				fileObj.thumb = active.thumbdir + fileObj.file + '.jpg';
				filesArr.push(fileObj);
				await createThumbs(fileObj);
			}
		}
		logger.debug(JSON.stringify(filesArr));
		res.locals.files = filesArr;
		next();
	} catch (e) {
		logger.warn(e);
		next(e);
	}
};

router.get('/', loadFiles, (req, res) => res.render('index'));

router.get('/player', (req, res) => {
	if (typeof active.file !== 'undefined') {
		res.locals.file = active.file;
		res.locals.mime = active.mime;
		res.locals.type = active.type;
	}
	res.render('player');
});

router.post('/', async (req, res) => {
	if (req.busboy) {
		req.busboy.on('file', (name, file, info) => {
			const fstream = fss.createWriteStream('public/' + info.filename);
			file.pipe(fstream);
			fstream.on('close', () => res.redirect('/'));
		});
		req.busboy.on('field', async (name, value) => {
			if (name === 'select') {
				active.file = value;
				active.mime = await mime.lookup(active.path + active.file);
				active.type = String(active.mime).split('/').shift();
				logger.debug('active: ' + JSON.stringify(active));
				io.to('client').emit('select', active);
			}

            if (name === 'default') {
                active.file = '/default.jpg'
                active.type = 'image'
                io.to('client').emit('select', active);
			}

			if (name === 'delete') {
				try {
					await fs.unlink(active.path + value);
					await fs.unlink(active.path + active.thumbdir + value + '.jpg');
				} catch (e) {
					logger.warn(e);
				}
			}

			res.redirect('/');
		});
		req.pipe(req.busboy);
	}
});

/* eslint-disable */
router.use((error, req, res, next) => res.status(500).send(error));
/* eslint-enable */
router.use((req, res) => res.sendStatus(404));

module.exports = router;

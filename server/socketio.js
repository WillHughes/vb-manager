const logger = require('./logger');
const {Server} = require('socket.io');

const io = new Server({
	serveClient: false
});

const onConnection = (socket) => {
	logger.debug(socket.id);
	socket.on('', (data) => {});
};

io.use((socket, next) => {
	socket.join('client');
	next();
});
io.on('connection', onConnection);

module.exports = io;

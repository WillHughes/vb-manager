# VB-Manager

Web application to manage and set virtual backgrounds.  Meant to be used with OBS or similar software.


## Install
Clone the repo 
```
git clone https://gitlab.com/WillHughes/vb-manager.git
cd vb-manager
```

Build the container with `docker build . -t vb-manager`

Start the container with `docker run -d --restart=always -p 8085:8085 -v c:/users/CoolPerson/backgrounds:/app/public vb-manager`
> Change the -v `c:/users/CoolPerson/backgrounds` path to your own

Application is now listening on http://localhost:8085/

Player is available on http://localhost:8085/player
> Set window to fullscreen for best experience

const {src, dest, series, task, watch} = require('gulp');
const concat = require('gulp-concat');
const del = require('del');
const cleancss = require('gulp-clean-css');
const eslint = require('gulp-eslint');
const nodemon = require('gulp-nodemon');
const prettier = require('gulp-plugin-prettier');
const sass = require('gulp-sass')(require('sass'));
const uglify = require('gulp-uglify');

task('clean', () => {
	return del(['dist/*/']);
});

task('css', () => {
	del(['dist/css/*/']);
	return src(['client/scss/style.scss'])
		.pipe(sass({quietDeps: true}))
		.pipe(cleancss({level: {1: {specialComments: 0}}}))
		.pipe(dest('dist/css'));
});

task('fonts', () => {
	del(['dist/webfonts/*/']);
	return src([
		'node_modules/@fontsource/lato/files/lato-latin-ext-400-normal.woff2',
		'node_modules/@fontsource/lato/files/lato-latin-400-normal.woff2',
		'node_modules/@fortawesome/fontawesome-free/webfonts/*.woff2'
	]).pipe(dest('dist/webfonts'));
});

task('player', () => {
	del(['dist/js/player.js']);
	return src(['node_modules/video.js/dist/video.min.js', 'client/js/player.js'])
		.pipe(concat('player.js'))
		.pipe(uglify())
		.pipe(dest('dist/js'));
});

task('vendor', () => {
	del(['dist/js/vendor.js']);
	return src([
		'node_modules/socket.io-client/dist/socket.io.min.js',
		'node_modules/jquery/dist/jquery.min.js',
		'node_modules/@popperjs/core/dist/umd/popper.min.js',
		'node_modules/bootstrap/dist/js/bootstrap.min.js'
	])
		.pipe(concat('vendor.js'))
		.pipe(uglify())
		.pipe(dest('dist/js'));
});

const watcher = (cb) => {
	watch(['client/js/player.js'], series('player'));

	const server = watch(['server/**/*.js', 'server.js']);
	server.on('change', (path) => {
		return src(path)
			.pipe(eslint())
			.pipe(eslint.format())
			.pipe(prettier.format());
	});

	watch(['client/scss/*.scss'], series('css'));

	nodemon({
		script: './server.js',
		ext: 'js',
		watch: ['server/', 'server.js'],
		done: cb
	});
};

exports.default = series('clean', 'css', 'fonts', 'player', 'vendor');
exports.watch = watcher;
